/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.testdbconnection;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
   private static final Logger LOGGER = Logger.getLogger(LoggerExample.class.getName());
   public  static  boolean debugFlag = true;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            DBConnector connector = DBConnector.getConnector();
            
            PropertyLoader pLoader = PropertyLoader.getLoader();
            
            Properties pp = pLoader.getPropValues( "dbConnector.properties" );
            
            connector.connect( pp );
        } 
        catch (IOException ex)
        {
            Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  static  void    log( String msg )
    {
        LOGGER.info( msg );
        System.out.println( msg );
    }
    
}
